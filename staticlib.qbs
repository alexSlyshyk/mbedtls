import qbs

StaticLibrary{
    Depends{name:"cpp"}
    targetName:{
        var tn = "mbedtls"
        if( cpp.compilerPathByLanguage["cpp"].contains("arm-none"))
            tn = tn + "-none"
        else if(qbs.targetOS.contains("linux"))
            tn = tn+"-linux"
        else if (qbs.targetOS.contains("windows"))
            tn = tn+"-win"


        tn = tn+"-"+qbs.architecture+"-mt"
        if(qbs.buildVariant == "debug")
            tn = tn+"d"
        return tn;
    }
    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
    }

    cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections"]
    cpp.cxxFlags:{
        var flags = base
        if(qbs.toolchain.contains("gcc"))
            flags = flags.concat(["-std=gnu++11"])

        return flags
    }

    cpp.cFlags:{
        var flags = base
        if(qbs.toolchain.contains("gcc"))
            flags = base.concat("-std=gnu11")

        return flags
    }

    cpp.includePaths:{
         var paths = base
         paths = paths.concat(["mbedtls-1.3.11/include"])
         return paths
     }

    cpp.defines:{
        var defs = base
        if( cpp.compilerPathByLanguage["cpp"].contains("arm-none"))
            defs = defs.concat(['POLARSSL_CONFIG_FILE="../../config/configbaremetal.h"'])

        return defs
    }

    Group{
        name:"include"
        prefix:"mbedtls-1.3.11/include/polarssl/"
        files:["*.h"]
    }
    Group{
        name:"config"
        prefix:"config/"
        files:["*.h"]
    }
    Group{
        name:"library"
        prefix:"mbedtls-1.3.11/library/"
        files:["*.c"]
    }
}//StaticLibrary
